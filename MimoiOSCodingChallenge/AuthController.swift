//
//  AuthController.swift
//  MimoiOSCodingChallenge
//
//  Created by orlando arzola on 6/28/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import Foundation
import Alamofire

@objc class AuthController: NSObject {
    
    class func loginOrSignUp(email: String, password: String, isLogin: Bool, completion: @escaping (_ response: NSDictionary?, _ error: String?) -> Void) {
        
        var body:[String: Any] = [:]
        var url = ""
        
        if isLogin {
            
            body = [
                "client_id" : Constants.API.clientId,
                "username" : email,
                "password" : password,
                "connection" : Constants.API.connection,
                "scope" : "openid profile email"
            ]
            
            url = Constants.API.signInURL
            
        } else {
            body = [
                "client_id" : Constants.API.clientId,
                "email" : email,
                "password" : password,
                "connection" : Constants.API.connection
            ]
            url = Constants.API.signUpURL
        }
        
        
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: Constants.API.header).validate().responseJSON { (response) in
            switch response.result {
                
            case .success(let value):
                
                if let value2 = value as? NSDictionary{
                    
                    if isLogin {
                        
                        UserDefaults.standard.set(value2["access_token"] as! String, forKey: "token")
                        
                       getUserProfile(accessToken: value2["access_token"] as! String, completion: { (dictionary) in
                        
                        completion(dictionary, nil)
                        
                       })
                        
                    } else {
                        
                        UserDefaults.standard.set(value2["_id"] as! String, forKey: "token")
                        UserDefaults.standard.set(value2["email"] as! String, forKey: "email")
                    }
                    
                }
                
            case .failure:
                
                let statusCode = response.response?.statusCode
                
                if statusCode == 401 {
                    
                    completion(nil, "Wrong email or password.")
                } else if statusCode == 400 {
                    completion(nil, "Email already exist")
                } else {
                    completion(nil, "Unkown error")
                }

            }
        }
    }
    
    class func logout (completion: @escaping (_ success: Bool)-> Void) {
        Alamofire.request(Constants.API.logout).validate().responseJSON { (response) in
            switch response.result {
                
            case .success(let value):
                print(value)
                completion(true)
            case .failure(let error):
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    class func getUserProfile(accessToken: String, completion: @escaping(_ dictionary: NSDictionary) ->Void ) {
        
        let header = [
            "Authorization" : "Bearer \(UserDefaults.standard.object(forKey: "token") ?? "no values")"
        ]
        Alamofire.request(Constants.API.userInfo, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).validate().responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                if let value2 = value as? NSDictionary{
                    UserDefaults.standard.set(value2["email"] as! String, forKey: "email")
                    UserDefaults.standard.set(value2["picture"] as! String, forKey: "picture")
                    
                    completion(value2)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    class func getAvatar(url: String, completion: @escaping(_ image: UIImage?)->Void) {
        
        Alamofire.request(url).validate().responseData { (data) in
            switch data.result {
                
            case .success(let value):
            
                let image = UIImage(data: value)
                
                completion(image)

            case .failure(let error):
                print(error.localizedDescription)
                
            }
        }
        
    }
    
}




