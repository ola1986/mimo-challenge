//
//  Created by Mimohello GmbH on 16.02.17.
//  Copyright (c) 2017 Mimohello GmbH. All rights reserved.
//
	
extension SettingsViewController : SettingsTableViewCellDelegate {
	
	func switchChangedValue(switcher: UISwitch) {
        
        if switcher.isOn {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DarkModeNotification"), object: "black")
             UserDefaults.standard.set("black", forKey: "darkMode")
            
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DarkModeNotification"), object: "white")
            
            UserDefaults.standard.set("white", forKey: "darkMode")
        }
	}
}
