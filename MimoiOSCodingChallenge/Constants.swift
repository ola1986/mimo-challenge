//
//  Constants.swift
//  MimoiOSCodingChallenge
//
//  Created by orlando arzola on 6/28/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import Foundation


struct Constants {
    
    struct API {
        static let signUpURL = "https://mimo-test.auth0.com/dbconnections/signup"
        
        static let signInURL = "https://mimo-test.auth0.com/oauth/ro"
        
        static let logout = "https://mimo-test.auth0.com/v2/logout"
        
        static let userInfo = "https://mimo-test.auth0.com/userinfo"
        
        static let clientId = "PAn11swGbMAVXVDbSCpnITx5Utsxz1co"
        
        static let connection = "Username-Password-Authentication"
        
        static let header = ["Content-Type" : "application/json"]
    }
}
