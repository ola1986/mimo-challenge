//
//  UIViewController+Navigation.swift
//  MimoiOSCodingChallenge
//
//  Created by orlando arzola on 6/28/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit

// Extension to navigate from and to the app when login and logout

extension UIViewController: CAAnimationDelegate {
    
    func enterApp() {
        
        let homeVC = SettingsViewController()
        
        setUpRootWindow(viewController: homeVC)
    }
    
    func exitApp() {
        
       
        let loginVC = LoginViewController()
        
        setUpRootWindow(viewController: loginVC)
        
    }
    
    private func setUpRootWindow(viewController: UIViewController) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.layer.add(setupAnimation(), forKey: "transitionViewAnimation")
        
        appDelegate.window?.rootViewController = viewController
        
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    private func setupAnimation() -> CATransition {
        
        let animation = CATransition()
        animation.delegate = self
        animation.type = kCATransitionFade
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        return animation
        
    }
}
