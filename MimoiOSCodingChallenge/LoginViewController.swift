//
//  LoginViewController.swift
//  MimoiOSCodingChallenge
//
//  Created by orlando arzola on 6/28/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit
import MBProgressHUD

@objc class LoginViewController: UIViewController {

    //Login Outlets properties
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginOrSignUpButton: UIButton!
    
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var changeLoginOrSignUpButton: UIButton!
    
    
    var isLogin = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        let loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        loading.mode = .indeterminate
        loading.label.text = "Loading..."
        
        let email = emailTextField.text
        let password = passwordTextField.text
    
        
        if (email?.isEmpty)! || (password?.isEmpty)! {
            
            self.createAlert(title: "Warning", message: "Email and password cannot be empty")
            
            MBProgressHUD.hide(for: self.view, animated: true)
        } else if !isValidEmail(email: email!){
            self.createAlert(title: "Warning", message: "You have to use a valid email")
            MBProgressHUD.hide(for: self.view, animated: true)
        } else {
            
            AuthController.loginOrSignUp(email: email!, password: password!, isLogin: isLogin, completion: { (response, error) in
                    
                MBProgressHUD.hide(for: self.view, animated: true)
                if response != nil {
                    
                    print(response ?? "no value")
                    
                    self.enterApp()
                    
                } else {
                    
                    print(error ?? "no error")
                    
                    self.createAlert(title: "Error", message: error!)
    
                }

            })
          
        }
        
        
    }
    
    @IBAction func showSignUp(_ sender: Any) {
        
        //Changing between sign in or sign up logic
        
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        
        if self.isLogin {
            self.loginOrSignUpButton.setTitle("Sign Up", for: .normal)
            self.changeLoginOrSignUpButton.setTitle("Sign In", for: .normal)
            self.accountLabel.text = "Have an account?"
            self.isLogin = false
        } else {
            
            self.loginOrSignUpButton.setTitle("Sign In", for: .normal)
            self.changeLoginOrSignUpButton.setTitle("Sign Up", for: .normal)
            self.accountLabel.text = "Don't have an account?"
            self.isLogin = true
        }
        
        
    }
    
    
    // Helper methods
  

    func createAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
            
        }))
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func isValidEmail(email:String) -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
}

extension LoginViewController {
    
    // Dismmising keyboard methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
        
    }
    
}
